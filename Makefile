PREFIX ?= /usr/local
VERSION = 1.0.0
DOCKERID = claughton

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/pdb2gif $(DESTDIR)$(PREFIX)/bin/pdb2gif

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/pdb2gif
	@docker rmi $(DOCKERID)/chimera:$(VERSION)
	@docker rmi $(DOCKERID)/chimera:latest

build: Dockerfile
	@docker build -t $(DOCKERID)/chimera:$(VERSION) . \
	&& docker tag $(DOCKERID)/chimera:$(VERSION) $(DOCKERID)/chimera:latest

publish: build
	@docker push $(DOCKERID)/chimera:$(VERSION) \
	&& docker push $(DOCKERID)/chimera:latest

.PHONY: all install uninstall build publish
