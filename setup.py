from setuptools import setup, find_packages
setup(
    name = 'chimeradocker',
    version = '1.0.0',
    packages = find_packages(),
    scripts = [
        'scripts/pdb2gif',
    ],
)
