from chimera import runCommand as rc
from argparse import ArgumentParser
import subprocess

parser = ArgumentParser()

parser.add_argument('pdbin', help='PDB file to convert')
parser.add_argument('gifout', help='GIF file name')
parser.add_argument('--commands', help='Chimera commands to run before images are made.')

args = parser.parse_args()

nmodels = 0
with open(args.pdbin) as f:
    for line in f.readlines():
        if 'MODEL' in line:
            nmodels += 1
rc('open noprefs 0 {}'.format(args.pdbin))
if args.commands:
    rc(args.commands)
if nmodels < 2:
    rc('copy file /tmp/img001.png')
else:
    rc('~modeldisp #0')
    for imodel in range(nmodels):
        rc('modeldisp #0.{}'.format(imodel + 1))
        rc('copy file /tmp/img{:03d}.png'.format(imodel + 1))
        rc('~modeldisp #0.{}'.format(imodel + 1))
        
result = subprocess.check_output('convert -dispose 3 -coalesce /tmp/img???.png {}'.format(args.gifout), shell=True)
rc('stop now')
