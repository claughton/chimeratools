# ChimeraTools

ChimeraTools provides a Dockerized version of UCSF Chimera in "headless" form.
The main purpose is to provide a route to using the image rendering capabilities
of Chimera in command line tools, or wrapped in Python functions.

In particular it is designed for use with [Pinda](https://claughton/bitbucket.io/pinda.html), and so in line with the version tagging system used by Pinda, 
please navigate to the `v1.0.0` branch of this repository for fuller details.
