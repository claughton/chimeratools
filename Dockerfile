FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y libfreetype6 libx11-6 libxext6 imagemagick
WORKDIR /app
COPY chimera-alpha-linux_x86_64_osmesa.bin  /app
COPY install.sh /app
RUN /app/install.sh
COPY pdb2gif.py /usr/local/bin
RUN rm /app/*
WORKDIR /wd
